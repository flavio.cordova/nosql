'use strict'

const mongoose = require('mongoose');

// Make Mongoose use the ES6 promise
mongoose.Promise = global.Promise;
// Connect to a local database called `feathers`

module.exports = function () {
  // Add your custom middleware here. Remember, that
  // just like Express the order matters, so error
  // handling middleware should go last.
  const app = this

  console.log("Conectando MongoDB em", app.get('mongodb_url'))
  mongoose.connect(app.get('mongodb_url'), { useNewUrlParser: true } )

  app.use('/api/locations', require('./locations'))
  app.use('/api/customers', require('./customers'))
  app.use('/api/orders', require('./orders').service)
  app.use('/api/calculateRoute', require('./calculateRoutes'))

  // register hooks
  app.service('/api/locations').hooks(require('./locations-graph'))
  app.service('/api/calculateRoute').hooks(require('./calculateRoutes.hooks.js'))
}
