const mongoose = require('mongoose');
const service = require('feathers-mongoose');

const Schema = mongoose.Schema;
const OrderSchema = new Schema({
  customer: {
    type: Schema.Types.ObjectId, 
    ref: 'Customer' 
  },
  from: {
    type: Schema.Types.ObjectId, 
    ref: 'Location' 
  },
  packages: [
    {
      destination: { 
        type: Schema.Types.ObjectId, 
        ref: 'Location' 
      },
      weight: Number
    }
  ]
});
const Model = mongoose.model('Order', OrderSchema);

module.exports = {
  service: service({ Model }),
  Model
}
