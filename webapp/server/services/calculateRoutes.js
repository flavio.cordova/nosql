const ObjectId = require('mongoose').Types.ObjectId
const OrderModel = require('./orders').Model

var feathersApp = null;

module.exports = {
  async find(params) {
    console.log(`#### Calculando rota de ${params.query.from} até ${params.query.to}`)

    var session = neo4jDriver.session();
    return session
      .run(`
      MATCH (n_from:Location {mongo_id: {id_from}}),
            (n_to:Location {mongo_id: {id_to}}),
            p = (n_from)-[r*]-(n_to)
        RETURN p AS trajeto,
               REDUCE(s = 0, r IN r | s + TOINT(r.distance)) AS distancia
        ORDER BY distancia
        LIMIT 1      
      `, 
        {
          id_from: params.query.from,
          id_to: params.query.to
        })
      .then(result => {
        session.close();
        
        var steps = result.records[0]._fields[0].segments;
        var cities = steps.map(s => ObjectId(s.start.properties.mongo_id))
        cities.push(ObjectId(steps[steps.length - 1].end.properties.mongo_id))

        var mongoAggregate = [
          {
            $match: {
              "from": {
                "$in": cities
              }
            }
          },
          {
            $unwind: "$packages"
          },
          {
            $match: {
              "packages.destination": {
                "$in": cities
              }
            }
          },
          {
            $project: {
                "_id":1,
                "customer": 1,
                "from": 1,
                "to": "$packages.destination",
                "weight": "$packages.weight"
            }
          },
          {
            $group: {
                "_id": {
                    "from": "$from", 
                    "to": "$to"
                },
                "packages": { 
                  "$push": "$$ROOT" 
                }
            }
          },
        ]

        return OrderModel.aggregate(mongoAggregate).exec()
          .then(mongoInfo => {
            // Precisa descartar os pacotes que estão na ordem inversa da viagem
            var finalPackages = []
            for(var stop = 0; stop < cities.length-1; stop++) {
              var departure = cities[stop].toString()
              var headingTo = cities.slice(stop+1).map(c => c.toString())

              // Filtra os pacotes que tem destino às próximas cidades
              finalPackages = finalPackages.concat(mongoInfo
                .filter(p => p._id.from.toString() === departure && headingTo.indexOf(p._id.to.toString()) >= 0))
            }
            return finalPackages
          })
          .then(packages => {
            // Cria um mapa com todas as cidades pra facilitar a busca
            var citiesMap = {}
            steps.forEach(c => citiesMap[c.start.properties.mongo_id] = c.start.properties.name)
            citiesMap[steps[steps.length-1].end.properties.mongo_id] = steps[steps.length-1].end.properties.name

            var pos = 0
            return cities.map(c => {
              var stopData = {
                from: citiesMap[c],
                distance: (pos > 0)?steps[pos-1].relationship.properties.distance:null,
                collect: packages.filter(p => p._id.from.toString() === c.toString()),
                to: (pos < cities.length-1)?citiesMap[cities[pos+1]]:null,
                deliver: packages.filter(p => p._id.to.toString() === c.toString())
              }
              pos++
              return stopData
            })
          })
          // Busca os dados dos clientes
          .then(planning => {
            var allCustomers = planning
              .reduce((accum, item) => accum.concat(item.collect), [])
              .reduce((accum, item) => accum.concat(item.packages), [])
              .map(item => item.customer)

            var customerService = feathersApp.service('/api/customers')
            return customerService.find(
              {
                query: {
                  _id: {
                    $in: allCustomers
                  }
                }
              })
              .then(customers => {
                var customersMap = {}
                customers.forEach(c => customersMap[c._id] = c.name)

                planning.forEach(stop => {
                  stop.collect.forEach(c => {
                    c.packages.forEach(p => {
                      p.customer = customersMap[p.customer]
                    })
                  })
                })
                return planning
              })
          })
      })
      .catch(function (error) {
        console.log('Error creating node:', error)
        return []
      });
  },
  setup(app, path) {
    feathersApp = app
  }
}