const redisBefore = require('feathers-hooks-rediscache').redisBeforeHook;
const redisAfter = require('feathers-hooks-rediscache').redisAfterHook;
const cache = require('feathers-hooks-rediscache').hookCache;
 
module.exports = {
  before: {
    find: [redisBefore()]
  },
 
  after: {
    find: [cache({duration: 3600 * 24 * 7}), redisAfter()]
  }
};