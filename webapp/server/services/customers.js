const mongoose = require('mongoose');
const service = require('feathers-mongoose');

const Schema = mongoose.Schema;
const CustomerSchema = new Schema({
  name: {
    type: String,
    required: true
  }
});
const Model = mongoose.model('Customer', CustomerSchema);

module.exports = service({ Model })