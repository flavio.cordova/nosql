const mongoose = require('mongoose');
const service = require('feathers-mongoose');

const Schema = mongoose.Schema;
const LocationSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  routes: [
    {
      target: { 
        type: Schema.Types.ObjectId, 
        ref: 'Location' 
      },
      distance: Number
    }
  ]
});
const Model = mongoose.model('Location', LocationSchema);

var loc_service = service({ Model })
module.exports = loc_service